﻿namespace HotelManagement.Models
{
    /// <summary>
    /// This is a class that displays the room on the view
    /// </summary>
    public class RoomViewModel
    {
        /// <summary>
        /// this is the unique id from the system
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// this is the Name created from the room saved in the system
        /// </summary>
        public string Name { get; set; }
    }
}
