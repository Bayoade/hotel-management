﻿namespace HotelManagement.Models
{
    public class SaveRoomInformationViewModel
    {
        /// <summary>
        /// this is the unique id from the system
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// This is the floor number of the hotel building
        /// </summary>
        public int Floor { get; set; }
    }
}
