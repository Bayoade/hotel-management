﻿using AutoMapper;
using Microsoft.Extensions.DependencyInjection;

namespace HotelManagement.Bootstrap
{
    public static class ConfigureAutoMapper
    {
        /// <summary>
        /// This method initializes the automapper profiles created different parts of the application
        /// </summary>
        /// <param name="services"></param>
        public static void AddAutoMapperProfiles(this IServiceCollection services)
        {
            Mapper.Initialize(config =>
            {
                config.AddProfile<WebMapperProfile>();
            });
        }
    }
}
