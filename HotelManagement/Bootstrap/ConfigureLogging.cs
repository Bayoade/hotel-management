﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Logging;
using Serilog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace HotelManagement.Bootstrap
{
    public static class ConfigureLogging
    {
        /// <summary>
        /// This method adds the global logger to the system in a file using serilog
        /// </summary>
        /// <param name="app"></param>
        /// <param name="env"></param>
        /// <param name="loggingFactory"></param>
        public static void AddLoggingServices(this IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggingFactory)
        {
            Log.Logger = new LoggerConfiguration()
              .Enrich.FromLogContext()
              .MinimumLevel.Debug()
              .WriteTo.File(Path.Combine(env.ContentRootPath, "hostelmanagement-log.txt"))
              .CreateLogger();


            loggingFactory.AddSerilog();
        }
    }
}
