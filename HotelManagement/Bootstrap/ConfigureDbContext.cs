﻿using HotelManagement.Data.Context;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace HotelManagement.Bootstrap
{
    public static class ConfigureDbContext
    {
        /// <summary>
        /// This method adds the configuration of the db context to the system
        /// </summary>
        /// <param name="services"></param>
        public static void AddDbContextConfiguration(this IServiceCollection services)
        {
            services.AddDbContext<HotelManagementDbContext>(options =>
            {
                options.UseSqlServer(IocContainer.Configuration.GetConnectionString("HotelManagementConnectionKey"));
            });
        }
    }
}
