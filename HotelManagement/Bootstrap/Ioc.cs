﻿using Microsoft.Extensions.Configuration;

namespace HotelManagement.Bootstrap
{
    public static class Ioc
    {
    }

    /// <summary>
    /// This class creates static objects some configuration created at the beginning of the application
    /// </summary>
    public static class IocContainer
    {
        public static IConfiguration Configuration { get; set; }
    }
}
