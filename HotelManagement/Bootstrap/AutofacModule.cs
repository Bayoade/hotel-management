﻿using Autofac;
using HotelManagement.AppService.Service;
using HotelManagement.Core.IRepository;
using HotelManagement.Core.IService;
using HotelManagement.Data.Repository;

namespace HotelManagement.Bootstrap
{
    public class AutofacModule : Module
    {
        /// <summary>
        /// The method override the Module of the autofac to register all services in the system
        /// </summary>
        /// <param name="builder"></param>
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<RoomInformationRepository>()
                .As<IRoomInformationRepository>()
                .InstancePerLifetimeScope();

            builder.RegisterType<RoomInformationService>()
                .As<IRoomInformationService>()
                .InstancePerLifetimeScope();
        }
    }
}
