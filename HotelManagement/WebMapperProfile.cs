﻿using AutoMapper;
using HotelManagement.Core.Models;
using HotelManagement.Models;

namespace HotelManagement
{
    public class WebMapperProfile : Profile
    {
        public WebMapperProfile()
        {
            CreateMap<RoomInformation, SaveRoomInformationViewModel>()
                .ReverseMap();

            CreateMap<Room, RoomViewModel>()
                .ReverseMap();
        }
    }
}
