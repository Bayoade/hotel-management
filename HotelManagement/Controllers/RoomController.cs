﻿using AutoMapper;
using HotelManagement.Core.IService;
using HotelManagement.Core.Models;
using HotelManagement.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace HotelManagement.Controllers
{
    public class RoomController : Controller
    {
        /// <summary>
        /// setting the interface IRoomInformation service as private readonly
        /// </summary>
        private readonly IRoomInformationService _roomInformationService;
        /// <summary>
        /// setting the interface Ilogger service as private readonly
        /// </summary>
        private readonly ILogger _logger;

        /// <summary>
        /// Injecting the interface IRoomInformation service in the constructor
        /// Injecting the interface Ilogger service in the constructor
        /// </summary>
        /// <param name="roomInformationService"></param>
        public RoomController(IRoomInformationService roomInformationService, ILogger<RoomController> logger)
        {
            _roomInformationService = roomInformationService;
            _logger = logger;
        }

        /// <summary>
        /// The method returns All the rooms in the system
        /// </summary>
        /// <returns></returns>
        public async Task<IActionResult> AllRooms()
        {
            _logger.LogInformation("Trying to get all the rooms in the system ");

            var rooms = await _roomInformationService.GetAllRoomsAsync();

            _logger.LogInformation("Successfully gets all the rooms in the system ");

            return View(Mapper.Map<List<RoomViewModel>>(rooms));
        }


        /// <summary>
        /// Create the endpoint for adding a new room to the system
        /// </summary>
        /// <returns></returns>
        public IActionResult Create()
        {
            _logger.LogInformation("Trying to create a view to save a new room in the system");

            return View();
        }

        /// <summary>
        /// Creates the post end point usins Save room information view model to save the new room in the system
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> Create(SaveRoomInformationViewModel model)
        {
            _logger.LogInformation("Trying to save a new room in the system");

            var roomInformation = Mapper.Map<RoomInformation>(model);

            var newId = await _roomInformationService.CreateRoomAsync(roomInformation);

            _logger.LogInformation("Successfully saved a new room in the system");

            return RedirectToAction("AllRooms");
        }

        /// <summary>
        /// Create the endpoint for editting a room coming the system with the room id has a parameter
        /// </summary>
        /// <param name="roomId"></param>
        /// <returns></returns>
        public async Task<IActionResult> Edit(int roomId)
        {
            _logger.LogInformation($"Trying to get a room of id { roomId } from the system");

            var roomInformation = await _roomInformationService.GetRoomById(roomId);

            _logger.LogInformation($"Successfully got the room of id { roomId } from the system");

            _logger.LogInformation("Trying to create a view to Edit a room in the system");

            return View(Mapper.Map<SaveRoomInformationViewModel>(roomInformation));
        }

        /// <summary>
        /// Creates the post end point using Save room information view model to update a room in the system
        /// </summary>
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> Edit(SaveRoomInformationViewModel model)
        {
            _logger.LogInformation($"Trying to edit a room of id { model.Id } in the system");

            var roomInformation = Mapper.Map<RoomInformation>(model);

            await _roomInformationService.UpdateRoomAsync(roomInformation);

            _logger.LogInformation($"Successfully updated the room of id { model.Id } in the system");

            return RedirectToAction("AllRooms");
        }


        /// <summary>
        /// Creates the end point using to remove a new room from the system using room id as parameter
        /// </summary>
        /// <param name="roomId"></param>
        /// <returns></returns>
        public async Task<IActionResult> Delete(int roomId)
        {
            _logger.LogInformation($"Trying to delete a room with id {roomId} from the system");


            await _roomInformationService.DeleteRoomAsync(roomId);

            _logger.LogInformation($"Successfully deleted the room with id {roomId} from the system");

            return RedirectToAction("AllRooms");
        }
    }
}
    