﻿using AutoMapper;
using HotelManagement.Core.Helper;
using HotelManagement.Core.IRepository;
using HotelManagement.Core.Models;
using HotelManagement.Data.Context;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HotelManagement.Data.Repository
{
    public class RoomInformationRepository : IRoomInformationRepository
    {
        private readonly HotelManagementDbContext _dbContext;

        public RoomInformationRepository(HotelManagementDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<int> CreateRoomAsync(RoomInformation information)
        {
            _dbContext.RoomInformations.Add(information);
            await _dbContext.SaveChangesAsync();
            return information.Id;
        }

        public Task DeleteRoomAsync(int roomId)
        {
            var roomInfo = new RoomInformation { Id = roomId };

            _dbContext.RoomInformations.Attach(roomInfo);

            _dbContext.RoomInformations.Remove(roomInfo);
            return _dbContext.SaveChangesAsync();
        }

        public async Task<IList<RoomInformation>> GetAllRoomsAsync()
        {
            return await _dbContext.RoomInformations.ToListAsync();
        }

        public Task<RoomInformation> GetRoomById(int roomId)
        {
            return _dbContext.RoomInformations.FirstOrDefaultAsync(x => x.Id == roomId);
        }

        public async Task UpdateRoomAsync(RoomInformation information)
        {
            var roomInfoDb = await _dbContext.RoomInformations.FirstOrDefaultAsync(x => x.Id == information.Id);

            roomInfoDb.Floor = information.Floor;

            _dbContext.RoomInformations.Update(roomInfoDb);

            await _dbContext.SaveChangesAsync();
        }
    }
}
