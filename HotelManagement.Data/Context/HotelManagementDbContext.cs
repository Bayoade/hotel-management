﻿using HotelManagement.Core.Models;
using Microsoft.EntityFrameworkCore;

namespace HotelManagement.Data.Context
{
    public class HotelManagementDbContext : DbContext
    {
        public HotelManagementDbContext(DbContextOptions<HotelManagementDbContext> options) : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasDefaultSchema("dbo");

            var roomInformationModelBuilder = modelBuilder.Entity<RoomInformation>().ToTable("RoomInformations");

            roomInformationModelBuilder.HasKey(x => x.Id);
        }

        internal DbSet<RoomInformation> RoomInformations { get; set; }
    }
}
