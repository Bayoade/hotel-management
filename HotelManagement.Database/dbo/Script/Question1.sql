﻿--Questions 1

--1
SELECT Cust.Name
FROM [dbo].[Customers] AS Cust
INNER JOIN [dbo].[Payment] AS Paymt ON Cust.Id = Paymt.Cust_Id
GROUP BY Cust.Name
HAVING COUNT(Paymt.Cust_Id) > 1;

--2
SELECT Cust.Name, SUM(Paymt.Amount)
FROM [dbo].[Payment] AS Paymt 
INNER JOIN [dbo].[Customers] AS Cust ON Cust.Id = Paymt.Cust_Id AND Paymt.DATE BETWEEN #01/01/2018 AND #31/12/2018
GROUP BY Cust.Name
HAVING COUNT(Paymt.Cust_Id) > 2;