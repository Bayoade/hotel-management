﻿using HotelManagement.Core.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace HotelManagement.Core.IRepository
{
    public interface IRoomInformationRepository
    {
        Task<IList<RoomInformation>> GetAllRoomsAsync();

        Task<int> CreateRoomAsync(RoomInformation information);

        Task UpdateRoomAsync(RoomInformation information);

        Task DeleteRoomAsync(int roomId);

        Task<RoomInformation> GetRoomById(int roomId);
    }
}
