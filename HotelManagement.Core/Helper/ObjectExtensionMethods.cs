﻿namespace HotelManagement.Core.Helper
{
    public static class ObjectExtensionMethods
    {
        public static bool IsNull(this object source)
        {
            return source == null;
        }
    }
}
