﻿using HotelManagement.Core.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace HotelManagement.Core.IService
{
    public interface IRoomInformationService
    {
        Task<IList<Room>> GetAllRoomsAsync();

        Task<int> CreateRoomAsync(RoomInformation information);

        Task UpdateRoomAsync(RoomInformation information);

        Task DeleteRoomAsync(int roomId);

        Task<RoomInformation> GetRoomById(int roomId);
    }
}
