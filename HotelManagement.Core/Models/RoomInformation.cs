﻿namespace HotelManagement.Core.Models
{
    public class RoomInformation
    {
        public int Id { get; set; }

        public int Floor { get; set; }
    }
}
