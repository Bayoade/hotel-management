﻿using HotelManagement.Core.IRepository;
using HotelManagement.Core.IService;
using HotelManagement.Core.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace HotelManagement.AppService.Service
{
    public class RoomInformationService : IRoomInformationService
    {
        private readonly IRoomInformationRepository _roomInformationRepository;

        public RoomInformationService(IRoomInformationRepository roomInformationRepository)
        {
            _roomInformationRepository = roomInformationRepository;
        }
        public Task<int> CreateRoomAsync(RoomInformation information)
        {
            return _roomInformationRepository.CreateRoomAsync(information);
        }

        public Task DeleteRoomAsync(int roomId)
        {
            return _roomInformationRepository.DeleteRoomAsync(roomId);
        }

        public async Task<IList<Room>> GetAllRoomsAsync()
        {
            var roomInformations = await _roomInformationRepository.GetAllRoomsAsync();

            return HandleRoomNumber(roomInformations);
        }

        public Task UpdateRoomAsync(RoomInformation information)
        {
            return _roomInformationRepository.UpdateRoomAsync(information);
        }

        public IList<Room> HandleRoomNumber(IList<RoomInformation> roomInformations)
        {
            var roomNumbers = new List<Room>();

            foreach (var room in roomInformations)
            {
                var stringBuilder = new StringBuilder();
                stringBuilder.Append("RM");
                stringBuilder.Append(room.Floor);
                if (room.Id.ToString().Length == 1)
                {
                    stringBuilder.Append("0");
                }

                stringBuilder.Append(room.Id);
                roomNumbers.Add(new Room { Id = room.Id, Name = stringBuilder.ToString() });
            }

            return roomNumbers;
        }

        public Task<RoomInformation> GetRoomById(int roomId)
        {
            return _roomInformationRepository.GetRoomById(roomId);
        }
    }
}
